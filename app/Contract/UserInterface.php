<?php

namespace App\Contract;

use Illuminate\Support\Collection;

interface UserInterface
{
   public function all();

   public function show($user);

   public function store($parms);

   public function update($parms, $user);

   public function destroy($user);
}
