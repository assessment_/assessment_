<?php

namespace App\Http\Controllers;

use App\Contract\UserInterface;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repository\UserRepository;
use GuzzleHttp\Psr7\Request;

class UserController extends Controller
{
    private $UserRepository;

    public function __construct(UserInterface $UserRepository)
    {
        $this->UserRepository = $UserRepository;
    }

    public function index()
    {
        $users = $this->UserRepository->all();
        return view('users.list', compact('users'));
    }

    public function show(User $User)
    {
        return $this->UserRepository->show($User);
    }

    public function store(Request $request)
    {
        return $this->UserRepository->store($request);
    }

    public function update(Request $request, User $User)
    {
        return $this->UserRepository->update($request, $User);
    }

    public function destroy($id)
    {
        return $this->UserRepository->destroy($id);
    }
}
