<?php

namespace App\Repository;

use App\Contract\UserInterface;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Support\Collection;

class UserRepository implements UserInterface
{

    public function all()
    {
        $user = User::all();
        return $user;
    }

    public function show($user)
    {
        return $user;
    }

    public function store($parms)
    {
        $sql = new User;
        $sql = $sql->create($parms);

        if ($sql) {
            return $sql;
        } else {
            return "there is an error please try again later";
        }
    }

    public function update($parms, $user)
    {

        $user->update($parms);
        if ($user) {
            return $user;
        } else {
            return "there is an error please try again later";
        }
    }

    public function destroy($User)
    {
        $sql = User::findOrFail($User);
        $sql->delete();
        if ($sql) {
            return true;
        } else {
            return false;
        }
    }
}
