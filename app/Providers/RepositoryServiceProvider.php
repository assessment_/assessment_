<?php

namespace App\Providers;

use App\Contract\UserInterface;
////
use Illuminate\Support\ServiceProvider;

use App\Repository\UserRepository;


/**
 * Class RepositoryServiceProvider
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserInterface::class, UserRepository::class);
    }
}
