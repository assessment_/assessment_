 <!-- Navbar -->
 <nav class="main-header navbar navbar-expand navbar-white navbar-light">
     <!-- Left navbar links -->
     <ul class="navbar-nav">
         <li class="nav-item">
             <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
         </li>
     </ul>

     <!-- Right navbar links -->
     <ul class="navbar-nav ml-auto">

         @php $locale = session()->get('locale'); @endphp
         <li class="nav-item d-none d-sm-inline-block">
             <a href="#" class="nav-link"> @switch($locale)
                 @case('es')
                 Spain
                 @break
                 @default
                 English
                 @endswitch
             </a>
         </li>

         <li class="nav-item dropdown">
             <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                 Language <span class="caret"></span>
             </a>

             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                 <a class="dropdown-item" href="lang/en">English</a>
                 <a class="dropdown-item" href="lang/es">Spanish</a>
             </div>
         </li>

     </ul>
 </nav>
 <!-- /.navbar -->