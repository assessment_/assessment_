@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h3> Users List </h3>
        <hr />
        <table id="myTable" class="table table-bordered">
            <thead>
                <tr>
                    <th>User id</th>
                    <th>User Email</th>
                    <th>User Name</th>
                    <th>User Type</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->type }}</td>

                    <td><a href="{{ url('user/edit') }}" class="btn btn-info btn-sm">Edit</a> | <a href="{{ url('user/delete') }}" class="btn btn-danger btn-sm">Delete</a></td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
    <div class="col-md-1"></div>
</div>
@endsection